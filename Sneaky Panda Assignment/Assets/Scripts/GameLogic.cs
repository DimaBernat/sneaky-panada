﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
using TMPro;

public class GameLogic : MonoBehaviour
{
     public TextMeshProUGUI titleText;
     public List<EnemyDB> enemysClosestDB; 

    Vector3 spawnPos;
    //public float[] enemyDistances;

    int layerMask = 1 << 8, currentWave = 1, totalEnemyAliveCap, maxEnemys, enemSpawned, aliveEnemys,currentPlayerTarget;

    float enemyHealth = 100, enemyDmg = 10, enemySpeed = 3, enemyRateOfFire = 1.5f, capedPercentage = 2.5f, percentageMelleeE = 0.7f;

    static GameLogic instance;

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        if (Player.instance == null)
        {
            Utility.InstaniteFromResource("DoNotMoveOrRename/Player");
        }
        spawnPos = new Vector3(0, 1, 48); // new Vector3(0, 1, 96);

        BeginWave();
    }

    void BeginWave()
    {
        //Calculate Amount of enemys to spawn
        CalculateEnemyWave();
        //Notify player about incomeing Enemy Wave
        Notificastion("Wave " + currentWave + "\n IS IMINENTE");
        //Hide Notify
        Invoke("TurnOffNotificastion", 3f);
        //Spawn Batch
        Invoke("BatchSpawn", 5f);
    }

    void CalculateEnemyWave()
    {
        totalEnemyAliveCap = Mathf.Min((5 + (int)(currentWave * 1.25f)), 30);
        maxEnemys = 5 + currentWave * 2;
    }

    void BatchSpawn()
    {
        //Spawn  Enemy if we didnt reach Max     
        if (enemSpawned < maxEnemys)
        {
            //Calculate how many to spawn this batch
            int howManyToSpawn = Mathf.Min(totalEnemyAliveCap, maxEnemys - enemSpawned);
            //Add enemy to live count
            enemSpawned += howManyToSpawn;
            aliveEnemys = howManyToSpawn;
            if (howManyToSpawn > 3)
            { //Spawn Range 30%
                SerialSpawning(howManyToSpawn - 1 - (int)(howManyToSpawn * percentageMelleeE), "DoNotMoveOrRename/RangeE",5);
            }
            else
            {// under 3 Just Spawn Mele
                aliveEnemys = howManyToSpawn-1;
                SerialSpawning(howManyToSpawn-1, "DoNotMoveOrRename/MeleeE",1);
                return;
            }
            //Now lets Spawn 70% Melee 
            SerialSpawning((int)(howManyToSpawn * percentageMelleeE), "DoNotMoveOrRename/MeleeE",2);
        }
    }

    void SerialSpawning(int howManyToSpawn, string resourcePath,int spawnPoint)
    {
        //lets make a copy as its faster to init Copys
        GameObject rangeEnemy = Utility.InstaniteFromResource(resourcePath);
        //Set at Spawn Positsion
        rangeEnemy.transform.position = spawnPos + new Vector3(UnityEngine.Random.Range(spawnPoint, -spawnPoint),0,0);
        //Add to the list for later distance calculastion
        enemysClosestDB.Add(new EnemyDB(rangeEnemy.transform, 0));
        for (int i = 0; i < howManyToSpawn; i++)
        {
            enemysClosestDB.Add(new EnemyDB(Instantiate(rangeEnemy).transform, 0));
            //Spread Them
            enemysClosestDB[i].enemyTran.position += new Vector3(UnityEngine.Random.Range(spawnPoint, -spawnPoint), 0, 0);
        }
        rangeEnemy = null; //Clear reference out
    }

    void Notificastion(string text)
    {
        titleText.enabled = true;
        titleText.text = text;
    }

    void TurnOffNotificastion()
    {
        titleText.enabled = false;
    }

    void WaveDiffculty()
    {
        enemyDmg *= 1.03f;
        enemyHealth *= 1.03f;
        enemySpeed *= 1.03f;
        enemyRateOfFire *= 1.03f;
    }

    void GameEnd()
    {
        print("Game end");
        GamePhase.NextPhase();
    }

    public void EndWave()
    {
        print("End Wave");
        enemSpawned = 0;
        aliveEnemys = 0;
        //Notify player about incomeing Enemy Wave
        Notificastion("You are the BEST Wave " + currentWave + "\n was obliterated");
        //Increament Wave
        currentWave++;
        WaveDiffculty();
        //Regen 10%Health
        Player.RegenHealth(); //TODO: Add SFX   
        //Lets Breath A little
        Invoke("BeginWave", 5f);
    }    

    public static void EnemyDied(Transform deadEnemy)
    {
        instance.aliveEnemys--;
        //Delte the enemyTransfer from the list
    
            instance.enemysClosestDB.Remove(instance.enemysClosestDB.Find(i => i.enemyTran == deadEnemy));
        
        print("Enemy Died" + instance.aliveEnemys);

        //End of mini wave or wave
        if (instance.aliveEnemys < 0)
        {
            if (instance.enemSpawned < instance.maxEnemys)
            {
                instance.BatchSpawn();
            }
            else
            {
                instance.EndWave();
            }
        }
    }

    public static void PlayerDied()
    {
        //Show Scor
        instance.Notificastion("Ow MagiKarp \nyou only cleared" + instance.currentWave + " wave your Scor:" + Score.GetScore.ToString());
        //Fhinsh Game in 5
        instance.Invoke("GameEnd", 5);
    }



    #region ------------------ Physics & Human Interaction ----------------- 

    //Speculating there a faster way to achive "Find closer enemy" then a for Loop but time wise cant creat it right now
    public static Transform FindClosestEnemy(Vector3 playerPos) //Called by Player on Update if he isnt busy 
    {
        if (instance.enemysClosestDB.Count == 0)//no enemys scenario
        {
            return null;
        }
        int closestEnemy = 0; //keep refrance which enemy is sortest without re-arrangeing the hole List\Array
        float shortestdistance = 1000; //keep sortestdistance value for compersiment
        for (int i = 0; i < instance.enemysClosestDB.Count; i++)
        {
            //Detirmen enemy distance from player and save it in array for later sorting
            instance.enemysClosestDB[i].distansToPlayer = Vector3.Distance(instance.enemysClosestDB[i].enemyTran.position, playerPos);
            if (instance.enemysClosestDB[i].distansToPlayer < shortestdistance) //Not Checking yet if he can shoot the target as I dont know how expansive to use Rays and thinking forwared if its 500 enemys
            {
                shortestdistance = instance.enemysClosestDB[i].distansToPlayer;
                closestEnemy = i;
            }
        }
        instance.currentPlayerTarget = closestEnemy;//Save enemy which is targeted for later
        return instance.enemysClosestDB[closestEnemy].enemyTran;
    }

    public static bool IsThereLineSight(Vector3 myPos, Vector3 targetPos, string tag)
    {
        RaycastHit hit;
        if (Physics.Raycast(myPos, (targetPos - myPos).normalized, out hit))
        {
            if (hit.collider.tag == tag)
            {
                return true;

            }
        }
        return false;
    }

    public static void SortEnemysByDistance()
    {
        instance.enemysClosestDB = instance.enemysClosestDB.OrderBy(x => x.distansToPlayer).ToList();
    }

    public static Transform IterateWhichEnemyInSight(Vector3 myPos)
    {
        for (int i = 1; i < instance.enemysClosestDB.Count; i++)
        {
            if (IsThereLineSight(myPos, instance.enemysClosestDB[i].enemyTran.position, "Enemy"))
            {
                instance.currentPlayerTarget = i; //Save enemy which is targeted for later
                return instance.enemysClosestDB[i].enemyTran;
            }
        }
        return null;
    }
    #endregion ------------------ Physics & Human Interaction ----------------- 

    public static float GetDmg { get { return instance.enemyDmg; } }
    public static Vector4 InitEnemyStats { get { return new Vector4(instance.enemyHealth, instance.enemySpeed, instance.enemyDmg, instance.enemyRateOfFire); } }

}

[System.Serializable]
public class EnemyDB
{
    public Transform enemyTran;
    public float distansToPlayer;

    public EnemyDB(Transform tran, float dis)
    {
        enemyTran = tran;
        distansToPlayer = dis;
    }
}
