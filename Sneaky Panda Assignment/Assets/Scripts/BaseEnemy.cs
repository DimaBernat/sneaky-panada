﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BaseEnemy : MonoBehaviour
{
    protected float p_Health , p_Dmg , p_Speed ,p_AtkRate  = 1;
    protected bool isAlive =true;
    protected Animator myAnim;
    protected UnityEngine.AI.NavMeshAgent navMeshAgent;

    protected CharactersState myState; //Idle,Run,Eat,Death,Spin


    protected void Start()
    {
        myAnim = transform.GetChild(0).GetComponent<Animator>();
        navMeshAgent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        SetVitals(GameLogic.InitEnemyStats);
        navMeshAgent.speed = p_Speed;
    }

    private void OnCollisionEnter(Collision collision)
    {

        if (collision.collider.tag == "Player" && collision.gameObject.layer == LayerMask.NameToLayer("Projectile"))
        {    
            p_Health -= collision.transform.GetComponent<Projectile>().m_Dmg;
            Destroy(collision.gameObject); //TODO: ADD SFX
                if (p_Health <= 0)
                {
                Score.Scoring(5);
                    myState = (CharactersState)3;
                    PlayAnim();
               
                    GameLogic.EnemyDied(transform);
                Destroy(gameObject,1.5f); //TODO Change to turn off and send back to pool
                //Stop pysics
                navMeshAgent.Stop();
                isAlive = false;
                GetComponent<Collider>().enabled = false;
                }
        }
    }

    protected void PlayAnim()
    {
        myAnim.Play(myState.ToString());
    }

    protected void LootAtPlayer()
    {
        transform.LookAt(Player.instance.transform.position);
    }

    protected void LookAtPlayer()
    {
        transform.LookAt(Player.instance.transform.position);
    }
    #region --------------- Setters Getter ------- 
    public CharactersState MyState
    {
        get { return myState; }
        set { myState = value; }
    }

   
    public void SetVitals(Vector4 vitals)
    {
        p_Health = vitals.x;//heal;
        p_Speed = vitals.y + Random.Range(-1,1);//speed;
        p_Dmg = vitals.z;//dmg;
        p_AtkRate = vitals.w; //Rate
    }

    #endregion --------------- Setters Getter ------- 
}
