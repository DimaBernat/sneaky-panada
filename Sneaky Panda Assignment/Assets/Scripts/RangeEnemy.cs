﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class RangeEnemy : BaseEnemy
{
    Vector3 movmentTarget;
    float reactionTimeStamp;
    // Start is called before the first frame update
    void Start()
    {
        base.Start();
        navMeshAgent = GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        if (isAlive)
        {
                if (reactionTimeStamp + p_AtkRate < Time.timeSinceLevelLoad)
                {
                reactionTimeStamp = Time.timeSinceLevelLoad;
                    //Check If enemy in Line of Sight
                    if (GameLogic.IsThereLineSight(transform.position + new Vector3(0,0.5f,0), new Vector3(Player.instance.transform.position.x, 0.5f, Player.instance.transform.position.z), "Player"))
                    {
                        Attack();
                    }
                    else
                    {
                        ChooseRandomPosHasLineOfSight();
                    MoveToTarget();
                }

                }
            
        }
    }

    bool ChooseRandomPosHasLineOfSight()
    {
        LookAtPlayer();
        for (int i = 1; i < 30; i += 4)
        {
            //make Random point
            Vector3 test = new Vector3(transform.localPosition.x + (Random.Range(-i, i)) / 3, 1, transform.localPosition.z + Random.Range(-i, i));
           //Check If there Line of sight from locastion
            if (GameLogic.IsThereLineSight(test, Player.instance.transform.position + Vector3.up, "Player"))
            {
                movmentTarget = test;
                return true;
            }
        }
        movmentTarget = Player.instance.transform.position;
        return false;
    }

    void MoveToTarget()
    {
        LookAtPlayer();
        //Play run Anim
        myState = (CharactersState)1;
        PlayAnim();
        //Walk to Target        
        navMeshAgent.SetDestination(movmentTarget);
        navMeshAgent.Resume();
    }

    void Attack()
    {
        LookAtPlayer();
        myState = (CharactersState)2;
        PlayAnim();
        navMeshAgent.Stop();
        //Shoot Projectile 
        GameObject missle = PoolObjects.CreateProjectile();
        missle.transform.rotation = transform.rotation;
        missle.transform.position = transform.position + transform.forward + new Vector3(0, 0.5f, 0);
        missle.GetComponent<Projectile>().Init(Player.instance.transform, p_Speed * 5, p_Dmg, false);
    }
}
