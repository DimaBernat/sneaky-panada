﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CharactersState {Idle,Run,Eat,Death,Bounce };
public class Player : MonoBehaviour
{
    //[Range(0.1f, 50)]
     float m_ProjectileSpeed = 20, m_MovmentSpeed = 20,m_AttackRate = 1f,m_UnTouchable = 1;
     Material myMat;
    CharactersState myState;

    Quaternion startQua, targetQua;
 
    float  atkSpeed, dmg = 50,timeStamp,health = 100;
    bool isUnTouchable,isAlive;

    Rigidbody myRigi;
    Transform targetTransform;
    Animator myAnim;
    AudioSource audioS;
    public static Player instance;
    
    void Awake()
    {
        instance = this;
        myRigi = GetComponent<Rigidbody>();
        myAnim = transform.GetChild(0).GetComponent<Animator>();
    }

    void Start()
    {
        audioS = gameObject.AddComponent<AudioSource>();
        audioS.clip = Resources.Load("DoNotMoveOrRename/Fish") as AudioClip;
        if (myMat == null)
        {
            myMat = Resources.Load("DoNotMoveOrRename/Mat_Goldfish") as Material;
        }
        audioS.playOnAwake = false;
        isAlive = true;
    }


    // Update is called once per frame
    void Update()
    {
        if (isAlive)
        {
            if (myState == (CharactersState)0 && Time.timeSinceLevelLoad > m_AttackRate + timeStamp)
            {
                SearchClosestEnemy();
            }
            if (myState == (CharactersState)4)
            {
                if (Time.timeSinceLevelLoad > 2 + timeStamp)
                {
                    myState = (CharactersState)0;
                    PlayAnim();
                }
            }
        }    
    }

    void TurnTowareds() 
    {
        transform.LookAt(targetTransform.position);        
    }

    void SearchClosestEnemy()
    {
        if (targetTransform == null)
        {
            targetTransform = GameLogic.FindClosestEnemy(transform.localPosition);//Find Closest Enemy

            if (targetTransform == null) //No More Enemys!! No problems
            {
                Dance();
                return;
            }
            else
            {
                //Now lets see if nothing in the way with nearest Bastered..ahh Enemy
                if (!GameLogic.IsThereLineSight(transform.position, targetTransform.position, "Enemy"))//TODO: Change tag to realy take from data base of tag so if some 1 changes Enemy tag nickname
                {
                    GameLogic.SortEnemysByDistance(); //Lets then sort enemy by distance from us
                    //Go Through all enemy and see who is in line of sight except the first as we wont be here if we could hit him
                    targetTransform = GameLogic.IterateWhichEnemyInSight(transform.localPosition); 
                    if (targetTransform == null) //No More Enemys That I can shoot 
                    {
                        Dance();
                        return;
                    }
                }
            }
        }
        else if(!targetTransform.gameObject.activeInHierarchy || !GameLogic.IsThereLineSight(transform.position, targetTransform.position, "Enemy"))
        {//Enemy Died or lost Sight
            targetTransform = null;
            SearchClosestEnemy();
            return;
        }
                Attack();
    }

    void Attack()
    {
        TimeStamp();
        TurnTowareds(); // Turn Towared nearest enemy
        myState = (CharactersState)2; 
        PlayAnim(); //Atk Anim
        //Audio
        if (!audioS.isPlaying)
        {
        audioS.Play();
        }       
        
        //Shoot Projectile
        GameObject missle = PoolObjects.CreateProjectile();
        missle.transform.position = transform.position + transform.forward + new Vector3(0, 0.5f, 0);
        missle.GetComponent<Projectile>().Init(targetTransform, m_ProjectileSpeed,dmg, true);

        myState = (CharactersState)0; //Go Back to Idle So we can shoot again after delay
    }

    void PlayAnim()
    {
        myAnim.Play(myState.ToString());
    }

    void Dance() //If No Enemy to shoot lets dane a little :)
    {
        TimeStamp();
        //myState = (CharactersState)4;
        //PlayAnim();
    }

    void TimeStamp()
    {
        timeStamp = Time.realtimeSinceStartup; //Init Delay for next attack
    }

    private void OnCollisionEnter(Collision collision)
    {
       
        if (collision.collider.tag == "Enemy")        //TODO: Add SFX
        {
                if (collision.gameObject.layer == LayerMask.NameToLayer("Projectile"))
                {
                    Destroy(collision.gameObject);
                }
            if (isUnTouchable||!isAlive) //invulnerable
            {
                return;
            }
            //Handle Health
            health -= GameLogic.GetDmg;
            //Handle Score
            Score.Scoring(-3);
            //Flash
            isUnTouchable = true;
            StartCoroutine(FlashUnTouchable());
            if (health <= 0) //Death
            {
      
                myState = (CharactersState)3;
                PlayAnim();
                isAlive = false;
                GameLogic.PlayerDied();
            }
        }
    }

    //Moveing Player from Vector of the Joystick
    public static void MovePlayer(Vector2 diracstion)
    {
        instance.targetTransform = null; //Null target so when we stop we will search for a new one
        instance.myState = (CharactersState)1;
        Vector3 newPos = new Vector3(diracstion.x, 0, diracstion.y);
        instance.myRigi.MovePosition(instance.transform.position+ newPos * instance.m_MovmentSpeed*Time.deltaTime);
        instance.transform.rotation = Quaternion.LookRotation(newPos);
        instance.PlayAnim();
    }

    public static void StopMoveing()
    {
        //Stop Runing Anim 
        instance.myState = (CharactersState)0;
        instance.PlayAnim();
    }

    public static void RegenHealth(float healPercentage = 1.1f)
    {
        instance.health =  Mathf.Min(100, healPercentage * 100);
    }

    IEnumerator FlashUnTouchable() //TODO: Add SFX
    {
        Color Original = myMat.GetColor("_Color");
        float f = 0;
        while (f < 1)
        {
            f += Time.deltaTime;
            myMat.SetColor("_Color", Color.Lerp(Original, Color.black, Mathf.PingPong(Time.time, 0.3f))); //Flash material
            yield return null;
        }
        myMat.SetColor("_Color", Original); //Set orignal Color
        isUnTouchable = false;
    }
}
