﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolObjects : MonoBehaviour
{


    public static GameObject CreateProjectile()
    {
        GameObject Projectile = Utility.InstaniteFromResource("DoNotMoveOrRename/Projectile/orb");
        Projectile.AddComponent<BoxCollider>();
        Projectile.AddComponent<Rigidbody>().useGravity = false;
        Projectile.AddComponent<Projectile>();
        return Projectile;
    }

}
