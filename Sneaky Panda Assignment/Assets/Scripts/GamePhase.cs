﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GamePhase : MonoBehaviour
{
    private enum m_GameState { MainMenu,Game,Score}
    m_GameState currentScreen;
    static GamePhase instance;
    // Start is called before the first frame update
    void Awake()
    {
        instance = this;
        DontDestroyOnLoad(this);
    }


    //Changes game screens as needed
    public void SetGameScreen(int i)
    {
        ChangeScreens(i);      

    }

    void ChangeScreens(int screen)
    {
        if (screen > (int)m_GameState.Score)
        {
            screen = 0;
        }
        SceneManager.LoadScene(screen);
    }

    public void ExiteApp()
    {
        Application.Quit();
    }

    public static void NextPhase(int screen = -1)
    {
        if (screen == -1)
        {
            instance.ChangeScreens((int)instance.currentScreen++);
            //TODO:
        }
    }
}