﻿

public static class Score 
{
    private static float currentScore;
    public static float GetScore { get { return currentScore; }  }

    public static void Scoring(int points)
    {
        currentScore += points;
    }
}
