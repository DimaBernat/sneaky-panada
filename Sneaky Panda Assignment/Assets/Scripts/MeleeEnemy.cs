﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MeleeEnemy : BaseEnemy
{
    Vector3 playerLastPos;
    float timeStamp, reactionTimeStamp, reactionDelay = 1f, arrivaleTime = 0.5f;
    // Start is called before the first frame update
    void Start()
    {
        base.Start();
       
    }

    // Update is called once per frame
    void Update()
    {
        //AI simple thinking
        if (isAlive)
        {
            switch (myState)
            {
                case CharactersState.Idle:
                    if (!IsInRangeToAttack())
                    {
                    MoveToTarget();
                    }
                    else
                    {
                        Attack();
                    }
                    break;
                case CharactersState.Run:
               //will start checking ever frame if he closer then 0.5 sec from target
                    if (Time.timeSinceLevelLoad > timeStamp + arrivaleTime)
                    {
                        if (IsInRangeToAttack())//Close Enought?? ATTACK
                        {
                            Attack();
                            return;
                        }
                    }
                    if (Time.timeSinceLevelLoad > reactionDelay + reactionTimeStamp)//Every now and then Check if player moved away from his orignal pos
                    {
                        reactionTimeStamp = Time.timeSinceLevelLoad;                       
                        if (Vector3.Distance(playerLastPos, Player.instance.transform.position) > 0.2f)
                        {
                       
                            MoveToTarget();
                        }
                    }
                    break;
                case CharactersState.Eat: //Attack
                        if (Time.timeSinceLevelLoad > reactionDelay + reactionTimeStamp)//Every now and then Check if player moved away from his orignal pos
                        {
                     
                        myState = (CharactersState)0;
                            reactionTimeStamp = Time.timeSinceLevelLoad;
                        }
                        break;
                case CharactersState.Death:
                    navMeshAgent.Stop();
                    break;
                case CharactersState.Bounce:
                    break;
                default:
                    break;
            }     
        }
    }


    void MoveToTarget()
    {
        //Set up to checking         
        arrivaleTime = 0.5f;

        //Play run Anim
        myState = (CharactersState)1;
        PlayAnim();
        //Walk to Target        
        navMeshAgent.SetDestination(Player.instance.transform.position);
        navMeshAgent.Resume();
        //Delay Checking ArrivaleTime so we will know when to attack without calculating distance every frame
        Invoke("CheckDestinastionArrivale", 0.1f);
    }

    bool IsInRangeToAttack()
    {
        return Vector3.Distance(transform.position, Player.instance.transform.position) < 3f ? true : false;
    }

    void CheckDestinastionArrivale()
    {
        timeStamp = Time.timeSinceLevelLoad;
        arrivaleTime = (Vector3.Distance(transform.position, Player.instance.transform.position) / navMeshAgent.desiredVelocity.magnitude)-0.5f;      
    }

    void Attack()
    {       
        LookAtPlayer();
        reactionTimeStamp = Time.timeSinceLevelLoad;       
        myState = (CharactersState)2;
        PlayAnim();
    }

    
}
