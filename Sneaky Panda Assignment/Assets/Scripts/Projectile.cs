﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public float m_Speed,m_Dmg;
    public Transform m_Target;
    public bool m_IsPlayer;

    string targetTag;
    bool isOn = true;

    void Update()
    {
        if (m_Target == null) //Destroyed Target
        {
            Destroy(gameObject);
            return;
        }
        if (isOn && m_IsPlayer)
        {//Move to target Enemy Could of changed it to what Range Enemy doing but out of time
        transform.position = Vector3.MoveTowards(transform.position, m_Target.position+ Vector3.up, Time.deltaTime * m_Speed);
        }
    }

    //Init Projectile
    public void Init(Transform tartgetTransfrom, float speed, float dmg, bool isPlayerProjectile)
    {
        m_Dmg = dmg;
        gameObject.gameObject.layer = LayerMask.NameToLayer("Projectile");
     
        gameObject.tag = isPlayerProjectile ? "Player" : "Enemy";
        isOn = true;
        m_Target = tartgetTransfrom;
        //Be free projectile to achive your dreams
        if (isPlayerProjectile)//Make Sure you hiting the first
        {
            m_IsPlayer = isPlayerProjectile;          
            m_Speed = speed;
        }
        else
        {
            GetComponent<Rigidbody>().AddForce( ( (tartgetTransfrom.position + new Vector3(0, 1f, 0))- transform.position)*0.5f,ForceMode.Impulse);
            //kill me if I didnt hit
            Destroy(gameObject, 5); //TODO: Convert to POOl
        }
 
    }
}

