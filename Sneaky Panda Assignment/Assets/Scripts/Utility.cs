﻿using UnityEngine;
using System;

public class Utility : MonoBehaviour
{

    public static GameObject InstaniteFromResource(string fileLoc)
    {
        return Instantiate(Resources.Load(fileLoc) as GameObject);
    }
}
